#ifndef SERVER_H
#define SERVER_H

#include <sys/types.h> // socket, bind, listen, accept, connect, send, recv
#include <sys/socket.h> /// socket, bind, listen, accept, connect, send, recv
#include <fcntl.h> // used for open()
#include <unistd.h> // used for read(), write(), close()
#include <errno.h> // used for perror()
#include <netdb.h> // socket data structure
#include <string.h>
#include <string>
#include <regex>          // used for name testing
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <netinet/in.h>
#include <arpa/inet.h>

using namespace std;

#define DEFAULT_PORT    "80"
#define BACK_LOG        500
#define BUFFER_SIZE     32768   // 32 KiB
#define RESPONSE_SIZE   4096    // 4 KiB
#define FILE_NAME_SIZE  10 
#define DELIMITERS      " \r\n"
#define REGEX           "[a-zA-Z0-9]*"

#define VERSION_HTTP    "HTTP/1.1"

//HTTP Codes we will need to implement
#define HTTP_CONTINUE              "100 Continue"
#define HTTP_OK                    "200 OK"
#define HTTP_CREATED               "201 Created"
#define HTTP_BAD_REQUEST           "400 Bad Request"
#define HTTP_FORBIDDEN             "403 Forbidden"
#define HTTP_NOT_FOUND             "404 Not Found"
#define HTTP_INTERNAL_SERVER_ERROR "500 Internal Server Error"

enum Request_type {EMPTY = 0, GET, PUT};

/* Server.hpp
Defines the class "Connection".
Connection contains information critical to the server such as IP and port. */
class Server{
private:
    // Server Socket
    int server_fd; // file descriptor. The actual socket
    struct sockaddr_in server_addr; // server address w/ socket name, ip, port, protocol

    // Client Socket
    int client_fd;
    struct sockaddr client_addr;
    socklen_t client_addr_len; // accept() is pass by reference so we can't use sizeof(client_addr)

    char buffer[BUFFER_SIZE];

    // Parser
    Request_type request_type; // stores client's request (GET, PUT)
    char file_name[FILE_NAME_SIZE+1]; // stores client's given file name
    long content_length; // saved content length from PUT

public:
    /*--------------------------------------------------------------------------
    Constructors
    --------------------------------------------------------------------------*/
    Server(const char* new_ip, const char* new_port = DEFAULT_PORT);
    Server(const Server &source); // copy constructor

    /*--------------------------------------------------------------------------
    Destructor
    --------------------------------------------------------------------------*/
    ~Server();

    /*--------------------------------------------------------------------------
    Operators
    --------------------------------------------------------------------------*/
    Server& operator=(const Server &source);

    /*--------------------------------------------------------------------------
    Helper Functions
    --------------------------------------------------------------------------*/
private:
                void    copy(const Server &source);
    unsigned    long    get_addr(const char* name_in);
                void    create_socket(const char* ip, const char* port);
                void    run_server();
                int     service_client();
                ssize_t to_buffer(const int fd);
                void    parse_request();
                void    do_get_request();
                void    do_put_request();
                int     test_name(char* str);
                void    send_response(int socket, const char* response, ssize_t content_len = 0);
};

void errno_kill(const char* id, bool kill); // static subroutine

#endif
